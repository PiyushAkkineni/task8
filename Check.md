package com.DailyTasks;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number of elements");
		int num = sc.nextInt();
		int a[]=new int [num];
		System.out.print("Enter the elements into array");
		
		for (int i = 0; i<num; i++)
		{
			a[i]=sc.nextInt();
		}
		
		System.out.print("Printing the reverse elements in the array: ");
		
		for (int i = a.length-1;i>=0;i--)
		{
			System.out.print(a[i]+" ");
		}

	}

}
